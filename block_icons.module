<?php

/**
 * @file
 * Adds the ability to assign an image to each block. A block's image is
 * displayed as an icon next to its title.
 */

/**
 * Implementation of hook_theme().
 */
function block_icons_theme() {
  return array(
    'block_icon' => array(
      'arguments' => array($block_icon),
    ),
  );
}

/**
 * Implementation of hook_form_alter().
 */
function block_icons_form_alter(&$form, &$form_state, $form_id) {
  // Block configuration forms
  if ($form_id == 'block_admin_configure' || $form_id == 'block_add_block_form') {
    // Setup default values
    $path = '';
    $preset = 'block_icon';
    $location = 'title';
    $position = 'left';

    // Get existing values (if any)
    if (isset($form['delta']['#value'])) {
      $module = $form['module']['#value'];
      $delta = $form['delta']['#value'];
      $icon_details = get_block_icon($module, $delta);

      if (!empty($icon_details)) {
        $path = $icon_details->path;
        $preset = $icon_details->preset;
        $position = $icon_details->position;

        // Set location to 'custom' for those who have upgraded from 6.x-1.0
        if (!empty($icon_details->location)) {
          $location = $icon_details->location;
        }
        else {
          $location = 'custom';
        }
      }
    }

    // Get icon path for preview
    if (!empty($preset) && module_exists('imagecache')) {
      $preview = imagecache_create_url($preset, $path);
    }
    else {
      $preview = url(NULL, array('absolute' => TRUE)) . $path;
    }

    // Add block icon form elements
    $form['#attributes'] = array(
      'enctype' => 'multipart/form-data'
    );
    $form['block_settings']['block_icon'] = array(
      '#type' => 'fieldset',
      '#title' => t('Block icon'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['block_settings']['block_icon']['block_icon_preview'] = array(
      '#value' => theme('image', $preview, 'Block icon', 'Block icon', array('class' => 'block-icon-preview', 'style' => 'float: right;'), FALSE),
    );
    $form['block_settings']['block_icon']['block_icon_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to existing icon'),
      '#description' => t('The path to the icon file relative to the site\'s base path. Must be a JPEG, PNG or GIF image.'),
      '#default_value' => $path,
      '#required' => FALSE,
    );
    $form['block_settings']['block_icon']['block_icon_upload'] = array(
      '#type' => 'file',
      '#title' => t('Upload a new icon'),
      '#description' => t('Upload a new icon from your computer. Must be a JPEG, PNG or GIF image. Overrides above path.'),
    );
    if (module_exists('imagecache')) {
      $options = _block_icons_get_presets();
      $form['block_settings']['block_icon']['block_icon_preset'] = array(
        '#type' => 'select',
        '#title' => t('Imagecache preset'),
        '#description' => t('An Imagecache preset to use for this icon.'),
        '#default_value' => $preset,
        '#options' => $options,
      );
    }
    $form['block_settings']['block_icon']['block_icon_location'] = array(
      '#type' => 'radios',
      '#title' => t('Icon location'),
      '#description' => t("Where the icon will be displayed in the block. If you select 'custom', you will need to call the 'block_icon' theme function from your theme's block.tpl.php file; see README.txt for details."),
      '#default_value' => $location,
      '#options' => array('title' => t('Title'), 'content' => t('Content'), 'custom' => t('Custom')),
    );
    $form['block_settings']['block_icon']['block_icon_position'] = array(
      '#type' => 'radios',
      '#title' => t('Icon position'),
      '#default_value' => $position,
      '#options' => array('left' => t('Left'), 'right' => t('Right')),
    );

    // Add custom validate and submit functions
    $form['#validate'][] = '_block_icons_configure_validate';
    $form['#submit'][] = '_block_icons_configure_submit';
  }

  // Block deletion form
  if ($form_id == 'block_box_delete') {
    // Remove entry from database when block is deleted
    $form['#submit'][] = '_block_icons_delete_submit';
  }
}

/**
 * Validate function for block configuration form.
 */
function _block_icons_configure_validate($form, &$form_state) {
  // Validate path
  if (!empty($form_state['values']['block_icon_path'])) {
    // Remove any preceding slashes
    $icon = ltrim($form_state['values']['block_icon_path'], '/');

    // Make sure file is an image and of the correct type
    $info = image_get_info(_block_icons_get_relative_path($icon));
    if (empty($info) || empty($info['extension'])) {
      form_set_error('block_icon_path', t('Please make sure the icon path points to a valid image file. Only JPEG, PNG and GIF images are allowed.'));
    }

    // Save changes to path (i.e. no preceding slashes)
    $form_state['values']['block_icon_path'] = $icon;
  }

  // Validate file upload
  $file = file_save_upload('block_icon_upload', array('file_validate_is_image' => array()));
  if (!empty($file)) {
    $form_state['values']['block_icon_upload'] = $file->filepath;
  }

  // Validate position
  if (empty($form_state['values']['block_icon_position'])) {
    form_set_error('block_icon_position', t('Please select a position for the block icon.'));
  }
}

/**
 * Submit function for block configuration form.
 */
function _block_icons_configure_submit($form, &$form_state) {
  // Get values
  $module = $form_state['values']['module'];
  $delta = $form_state['values']['delta'];
  $path = $form_state['values']['block_icon_path'];
  $upload = $form_state['values']['block_icon_upload'];
  $preset = $form_state['values']['block_icon_preset'];
  $location = $form_state['values']['block_icon_location'];
  $position = $form_state['values']['block_icon_position'];

  // If delta is empty, get it from boxes table (user-created blocks only)
  if (!isset($delta)) {
    $sql = "SELECT bid FROM {boxes} WHERE info = '%s'";
    $result = db_fetch_object(db_query($sql, $form_state['values']['info']));
    $delta = $result->bid;
  }

  // Copy uploaded file from temp directory
  if (!empty($upload)) {
    // Create block_icons directory if it doesn't already exist
    $destination = file_directory_path() .'/block_icons';
    if (file_check_directory($destination, FILE_CREATE_DIRECTORY)) {
      // Copy uploaded file to block_icons directory
      $copied = file_copy($form_state['values']['block_icon_upload'], $destination);
      if ($copied) {
        $path = $form_state['values']['block_icon_upload'];
      }
    }
  }

  // Make necessary changes to database
  if (empty($path)) {
    // If path is empty, delete existing record
    $sql = "DELETE FROM {block_icons} WHERE module = '%s' AND delta = '%s'";
    db_query($sql, $module, $delta);
  }
  else {
    // Check for existing path
    $sql = "SELECT path FROM {block_icons} WHERE module = '%s' AND delta = '%s'";
    $result = db_fetch_object(db_query($sql, $module, $delta));

    $record = new stdClass();
    $record->module = $module;
    $record->delta = $delta;
    $record->path = $path;
    $record->preset = $preset;
    $record->location = $location;
    $record->position = $position;

    if (empty($result)) {
      // Add new record
      drupal_write_record('block_icons', $record);
    }
    else {
      // Update existing record
      drupal_write_record('block_icons', $record, array('module', 'delta'));
    }
  }
}

/**
 * Submit function for block deletion form.
 */
function _block_icons_delete_submit($form, &$form_state) {
  // Get values
  $module = 'block';
  $delta = $form_state['values']['bid'];

  // Delete database record when block is deleted
  $sql = "DELETE FROM {block_icons} WHERE module = '%s' AND delta = '%s'";
  db_query($sql, $module, $delta);
}

/**
 * Get icon details for the specified block.
 */
function get_block_icon($module, $delta = '0') {
  // Get icon details from database
  $sql = "SELECT path, preset, location, position FROM {block_icons} WHERE module = '%s' AND delta = '%s'";
  $result = db_fetch_object(db_query($sql, $module, $delta));

  return $result;
}

/**
 * Get a list of Imagecache presets.
 */
function _block_icons_get_presets() {
  // Create NULL option
  $options = array('' => t('- None -'));

  // Get a list of the names of all Imagecache presets
  $presets = imagecache_presets();
  foreach ($presets as $preset) {
    $options[$preset['presetname']] = $preset['presetname'];
  }

  return $options;
}

/**
 * Implementation of hook_imagecache_default_presets().
 */
function block_icons_imagecache_default_presets() {
  $presets = array();

  // Create default Imagecache preset for block icons
  $presets['block_icon'] = array(
    'presetname' => 'block_icon',
    'actions' => array(
      0 => array(
        'weight' => '0',
        'module' => 'imagecache',
        'action' => 'imagecache_scale',
        'data' => array(
          'width' => '30',
          'height' => '30',
        ),
      ),
    ),
  );

  return $presets;
}

/**
 * Override template_preprocess_block().
 */
function block_icons_preprocess_block(&$variables) {
  // Get values
  $module = $variables['block']->module;
  $delta = $variables['block']->delta;
  $icon_details = get_block_icon($module, $delta);
  $path = $icon_details->path;
  $preset = $icon_details->preset;
  $location = $icon_details->location;
  $position = $icon_details->position;

  if (!empty($path)) {
    // Get full path for icon
    if (!empty($preset) && module_exists('imagecache')) {
      $path = imagecache_create_url($preset, $path);
    }
    else {
      $path = url(NULL, array('absolute' => TRUE)) . $path;
    }

    // Setup array of icon variables
    $block_icon = image_get_info(_block_icons_get_relative_path($path));
    $block_icon['path'] = $path;
    $block_icon['preset'] = $preset;
    $block_icon['location'] = $location;
    $block_icon['position'] = $position;
    $block_icon['block_class'] = 'has-block-icon block-icon-'. $location .' block-icon-'. $position;
    $block_icon['module'] = $module;
    $block_icon['delta'] = $delta;

    // Add icon variables to block so themers can use them
    $variables['block_icon'] = $block_icon;

    // Inject block icon theme function into the desired location
    switch ($block_icon['location']) {
      case 'title':
        $variables['block']->subject = theme('block_icon', $variables['block_icon']) . $variables['block']->subject;
      break;
      case 'content':
        $variables['block']->content = theme('block_icon', $variables['block_icon']) . $variables['block']->content;
      break;
    }
  }
}

/**
 * Theme function for block_icon.
 */
function theme_block_icon($block_icon) {
  // Only output HTML if there's an icon for this block
  if (empty($block_icon['path'])) {
    return NULL;
  }

  // Get margins based on icon's position
  if ($block_icon['position'] == 'left') {
    $margin = '0 5px 0 0';
  }
  else {
    $margin = '0 0 0 5px';
  }

  // Setup default CSS for displaying icon next to block title
  $css = '<style type="text/css">
    #block-'. $block_icon['module'] .'-'. $block_icon['delta'] .' .block-icon {
      background: transparent url("'. $block_icon['path'] .'") center center no-repeat;
      float: '. $block_icon['position'] .';
      height: '. $block_icon['height'] .'px;
      margin: '. $margin .';
      width: '. $block_icon['width'] .'px;
    }
  </style>';

  // Add default CSS to page
  drupal_set_html_head($css);

  // Output HTML to display icon
  $output = '<div class="block-icon"></div>';
  return $output;
}

/**
 * Get relative path for use with image_get_info().
 */
function _block_icons_get_relative_path($path) {
  $root = url(NULL, array('absolute' => TRUE));
  return str_replace($root, '', $path);
}

/**
 * Recursively delete all files and folders from a specified path.
 * Taken from Drupal 7: file_unmanaged_delete_recursive().
 */
function _block_icons_delete_recursive($path) {
  if (is_dir($path)) {
    $dir = dir($path);
    while (($entry = $dir->read()) !== FALSE) {
      if ($entry == '.' || $entry == '..') {
        continue;
      }
      $entry_path = $path . '/' . $entry;
      _block_icons_delete_recursive($entry_path);
    }
    $dir->close();
    return rmdir($path);
  }

  if (is_file($path)) {
    return unlink($path);
  }
}
